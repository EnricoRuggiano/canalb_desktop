#!/bin/sh

# setup the desktop template
THIS_DIR=$(pwd)
FINDANDREPLACE='s#THIS_PATH#'$(echo $THIS_DIR)#
sed $FINDANDREPLACE canalb_template.desktop > canalb.desktop
chmod +x canalb.desktop

#check if it is a valid desktop
desktop-file-validate canalb.desktop

#install
desktop-file-install --dir=/home/$(echo $USER)/.local/share/applications/  canalb.desktop
