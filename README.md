# Canal B Desktop-ish application

Create an application link that opens the radio streaming of [Canal B Radio](https://canalb.fr/).

## Requirements
```
vlc
```

## Linux

Create a desktop custom application link for Ubuntu Gnome. Just run
```
./install.sh
```

### Check Canal B Streaming Link 

Check the [official website](https://canalb.fr/) if their stream link is working properly and remember to update the stream link in `canalb.m3u`

